local battle_mark = nil
local battle_sum = 0
function battle_sum_set()
	battle_mark = true
	memory.setregister("a", battle_sum)
end
memory.registerexecute(0xfc2e, 1, battle_sum_set)

local rgn_index_0016 = 0
local lottery_mark = 0
function lottery_bank()
	return memory.readbyte(0xbc45) == 0x8d
end
function lottery_start()
	if lottery_bank() == false then
		do return end
	end
	memory.writebyte(0x0016, rgn_index_0016)
	lottery_mark = 1
end
memory.registerexecute(0xbc45, 1, lottery_start)
function lottery_end()
	if lottery_bank() == false then
		do return end
	end
	lottery_mark = 2
end
memory.registerexecute(0xbd06, 1, lottery_end)
--RGN table 0x7be3-0x7ce2
--RGN table seed PC $fc2e, sum bit:4
--item drop RGN table index $0016
--bpset fc16,b@0021==1,{printf "PC:%04x a:%02X x:%02x", pc, a, x;go}
--抽選開始 $bc45
--抽選終了 $bd06

function lottery_loop(sum)
	battle_sum = sum
	local s = savestate.object()
	while lottery_mark == 0 do
		savestate.save(s)
		emu.frameadvance()
	end
	
	rgn_index_0016 = 0
	while rgn_index_0016 < 0x100 do
		savestate.load(s)
		while lottery_mark == 1 do
			emu.frameadvance()
		end
		local qty = memory.readbyte(0x60c1)
		if qty >= 2 then
			print(string.format("<$16:0x%02x, qty:%d", rgn_index_0016, qty))
		end
		lottery_mark = 1
		rgn_index_0016 = rgn_index_0016 + 1
	end
end

function str_to_key(str)
	local k = {}
	for i = 1, #str, 1 do
		s = string.upper(string.sub(str, i, i))
		local x = s
		if s == "S" then
			x = "select"
		elseif s == "T" then
			x = "start"
		elseif s == "U" then
			x = "up"
		elseif s == "D" then
			x = "down"
		elseif s == "L" then
			x = "left"
		elseif s == "R" then
			x = "right"
		end
		k[x] = true
	end
	return k
end

function key_to_byte_reverse(k)
	local kk = 0
	local v = 0x01
	for i, name in ipairs({"A", "B", "select", "start", "up", "down", "left", "right"}) do 
		if k[name] == true then
			kk = OR(kk, v)
		end
		v = v * 2
	end
	return kk
end

function battle_input(key)
	for i, k in ipairs(key) do
		local keytable = str_to_key(k)
		local keybyte = key_to_byte_reverse(keytable)
		while memory.readbyte(0x0013) ~= keybyte and memory.readbyte(0x0072) ~= 0x46 do
			joypad.set(1, keytable)
			emu.frameadvance()
		end
	end
end

--sum.bit.1 = 1; $0016=0xfd でポーションが3個おちる
function goblin_vs_command(goblin_qty)
	battle_input({"A"})
	local s = savestate.object()
	local command = {
		{"", "A", "L", "A"},
		{"", "A", "L", "D", "A"},
		{"", "A", "", "A"},
		{"R", "", "R", "A"},
		{"D", "A"},
		{"D", "", "D", "A"},
	}
	savestate.save(s)
	local framecount_prev = emu.framecount()
	for p0 = 1,6 do
	for p1 = 1,6 do
	for p2 = 1,6 do
	for p3 = 1,6 do
		local attack = 0
		if p0 < 4 then
			attack = attack + 1
		end
		if p1 < 4 then
			attack = attack + 1
		end
		if p2 < 4 then
			attack = attack + 1
		end
		if p3 < 4 then
			attack = attack + 1
		end
		if attack >= goblin_qty then
			savestate.load(s)
			battle_input(command[p0])
			battle_input(command[p1])
			battle_input(command[p2])
			battle_input(command[p3])
			for i = 0, 28 do
				emu.frameadvance()
			end
			local v = 0
			for i = 0, 4 do
				v = OR(v, memory.readbyte(0x7acb + i))
			end
			v = AND(v, 0x80)
			if v == 0x80 then
				--do return end
			end
			local battle_end = true
			while memory.readbyte(0x7678) ~= 0 or memory.readbyte(0x76b8) ~= 0 or memory.readbyte(0x76f8) ~= 0 do
				if memory.readbyte(0x0072) == 0x46 then
					battle_end = false
					break
				end
				if memory.readbyte(0x001c) == 0xa8 and memory.readbyte(0x001d) == 0x28 then
					battle_end = false
					break
				end
				emu.frameadvance()
			end
			if battle_end == true and (
				((goblin_qty == 3 and AND(battle_sum, 0x02) ~= 0 and memory.readbyte(0x0016) == 0xfd) or memory.readbyte(0x0016) == 0xfe) or
				((goblin_qty == 2 and AND(battle_sum, 0x02) ~= 0 and memory.readbyte(0x0016) == 0xfd) or memory.readbyte(0x0016) == 0xff)
			) then
				print(string.format("rgnindex:%02x, %d.%d.%d.%d, %d frames", memory.readbyte(0x0016), p0, p1, p2, p3, emu.framecount() - framecount_prev))
			end
		end
	end
	end
	end
	end
	return 
end
function goblin_fight(goblin_qty)
	emu.speedmode("turbo")
	local s = savestate.object()
	while battle_mark == nil do
		savestate.save(s)
		emu.frameadvance()
	end
	battle_sum = 0xe8
	while battle_sum < 0xf8 do
		local flag = true
		while flag == true do
			savestate.load(s)
			if goblin_qty == 3 and AND(2, battle_sum) == 0 then
				break
			end
			while memory.readbyte(0x7678) == 0 do
				emu.frameadvance()
			end
			emu.frameadvance()
			if goblin_qty == 2 and memory.readbyte(0x76f8) ~= 0 then
				break
			end
			if goblin_qty == 3 and memory.readbyte(0x76f8) == 0 then
				break
			end
			if goblin_qty == 3 and memory.readbyte(0x7738) ~= 0 then
				break
			end
			emu.frameadvance()
			print(string.format("sum=0x%02x", battle_sum))
			flag = false
			goblin_vs_command(goblin_qty)
		end
		battle_sum = battle_sum + 1
	end
end

--ゴブリン2匹--
--bit2 == 0
--<$16:0xff, qty:2
--bit2 == 1
--<$16:0xfd, qty:2
--<$16:0xff, qty:2
--ゴブリン3匹--
--bit2 == 0
--<$16:0x12, qty:2
--<$16:0x45, qty:2
--<$16:0xfe, qty:2
--<$16:0xff, qty:2
--bit1 == 1
--<$16:0x77, qty:2
--<$16:0xfc, qty:2
--<$16:0xfd, qty:3
--<$16:0xfe, qty:2
--<$16:0xff, qty:2


goblin_fight(2)
--lottery_loop(0xd7)
--lottery_loop(0xe2)
emu.pause()
