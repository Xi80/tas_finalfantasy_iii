LAYOUT_HOGE = <<EOS
ポーション:1 かわのたて:13
ふく:2 かわよろい:1
かわのぼうし:4 どうのうでわ:4
ポイゾナ:4 ロングソード:1
つえ:7 ヌンチャク:2
ダガー:12
EOS
LAYOUT_BEFORE = <<EOS
ポーション:1 かわのたて:13
ふく:3 かわよろい:1
どうのうでわ:1 かわのぼうし:5
ポイゾナ:4 ヌンチャク:2
つえ:7 ロングソード:4
ダガー:12 
EOS

LAYOUT_AFTER = <<EOS
ダガー:12 かわのたて:11
(空欄):0 (空欄):0
(空欄):0 (空欄):0
(空欄):0 ロングソード:1
(空欄):0 (空欄):0
ポーション:1 (空欄):0
(空欄):0 (空欄):0
(空欄):0 (空欄):0
ふく:3 かわよろい:1
(空欄):0 どうのうでわ:4
かわのぼうし:5 (空欄):0
ヌンチャク:2 ポイゾナ:4
つえ:7 (空欄):0
EOS

class String
	def mb_ljust(width, padding=' ')
		output_width = each_char.map{|c| c.bytesize == 1 ? 1 : 2}.reduce(0, &:+)
		padding_size = [0, width - output_width].max
		self + padding * padding_size
	end
end
=begin
1画面で 0 から 15, までで 8 行.
1回のアイテム移動で14 frame消費
1回のスクロールで 8 frame 消費
=end
=begin
 0:ポーション      : 1  1:かわのたて      :13
 2:ふく            : 2  3:かわよろい      : 1
 4:かわのぼうし    : 4  5:どうのうでわ    : 4
 6:ポイゾナ        : 4  7:ロングソード    : 1
 8:つえ            : 7  9:ヌンチャク      : 2
10:ダガー          :12
0->10 ダガー

 2:ふく            : 2  3:かわよろい      : 1
 4:かわのぼうし    : 4  5:どうのうでわ    : 4
 6:ポイゾナ        : 4  7:ロングソード    : 1
 8:つえ            : 7  9:ヌンチャク      : 2
10:ポーション      : 1
12
14
16
16->2 ふく, 3->17 かわよろい

 4:かわのぼうし    : 4  5:どうのうでわ    : 4
 6:ポイゾナ        : 4  7:ロングソード    : 1
 8:つえ            : 7  9:ヌンチャク      : 2
10:ポーション      : 1
12
14
16:ふく            : 2 17:かわよろい      : 1
18
19->5 どうのうでわ, 4->20: かわのぼうし

 6:ポイゾナ        : 4  7:ロングソード    : 1
 8:つえ            : 7  9:ヌンチャク      : 2
10:ポーション      : 1
12
14
16:ふく            : 2 17:かわよろい      : 1
18:                    19:どうのうでわ    : 4
20:かわのぼうし    : 4
6->23 ポイゾナ, 22->9ヌンチャク, 8->24:つえ
=end
=begin
12:empty 13:ロングソード:4
14:empty 15:empty
16:empty 17:X:1
18:N:3 19:N:3
20:N:1 21:N:2
22:ポイゾナ:4 23:N:7

 0:ポーション      : 1  1:かわのたて      :13
 2:ふく            : 3  3:かわよろい      : 1
 4:どうのうでわ    : 1  5:かわのぼうし    : 5
 6:ポイゾナ        : 4  7:ヌンチャク      : 2
 8:つえ            : 7  9:ロングソード    : 4
10:ダガー          :12
12
14
0->10 ポーション-ダガー, 9->13 ロングソード

 2:ふく            : 3  3:かわよろい      : 1
 4:どうのうでわ    : 1  5:かわのぼうし    : 5
 6:ポイゾナ        : 4  7:ヌンチャク      : 2
 8:つえ            : 7
10:ダガー          :12
12                     13:ロングソード    : 4
14
16
17->3 かわよろい 2->18 ふく

 4:どうのうでわ    : 1  5:かわのぼうし    : 5
 6:ポイゾナ        : 4  7:ヌンチャク      : 2
 8:つえ            : 7
10:ダガー          :12
12                     13:ロングソード    : 4
16                     17:かわよろい      : 1
18:ふく            : 3
19->5かわのぼうし, 4->20 どうのうでわ

 6:ポイゾナ        : 4  7:ヌンチャク      : 2
 8:つえ            : 7
10:ダガー          :12
12                     13:ロングソード    : 4
16                     17:かわよろい      : 1
18:ふく            : 3 19:かわのぼうし    : 5
20:どうのうでわ    : 1
21->7 ヌンチャク, 6->22 ポイゾナ, 23->8 つえ
=end
def layout(list)
	str = ""
	i = 0
	j = 0
	list.split(/[ \n]/).each{|t|
		item = t.split(':')
		str << sprintf("%2d:%s:%2d ", i, item[0].mb_ljust(16), item[1].to_i)
		i += 1
		j += 1
		if j >= 2
			j = 0
			puts str.rstrip
			str = ""
		end
	}
	if str != ""
		puts str
	end
end

def move_cursor(t, target)
	if t[:cursor] == target
		if t[:key].last == "A"
			t[:key] << ""
		end
		t[:key] << "A"
		return
	end
	param = {1 => "R", 2 => "D", :mul => 1}
	if target < t[:cursor]
		param = {1 => "L", 2 => "U", :mul => -1}
	end
	if (t[:cursor] - target).abs == 1
		if t[:key].last.include?(param[1]) ||t[:key].last.include?("A")
			t[:key] << ""
		end
		t[:key] << (param[1] + "A")
	elsif (t[:cursor] - target).abs == 2
		if t[:key].last.include?(param[2]) ||t[:key].last.include?("A")
			t[:key] << ""
		end
		t[:key] << (param[2] + "A")
	else
		cursor = t[:cursor]
		while (cursor - target).abs >= 2
			if t[:key].last.include?(param[1])
				t[:key] << (param[2])
			else
				t[:key] << (param[2] + param[1])
			end
			cursor += 2 * param[:mul]
		end
		case (cursor - target).abs
		when 0
			t[:key][t[:key].size - 1] += "A"
		when 1
			t[:key] << (param[1] + "A")
		end
	end
	t[:cursor] = target
end
def move_item(t, src, dest)
	move_cursor(t, src)
	move_cursor(t, dest)
end

begin
	layout LAYOUT_BEFORE
	layout LAYOUT_AFTER
	t = {:cursor => 0, :key => ["A"]}
	[
#254 frames
#		[0,10],[16,2],[3,17],
#		[19,5],[4,20],
#		[6,23],[22,9],[8,24]
		[0,10],[9,13],
		[17,3],[2,18],
		[19,5],[4,20],
		[21,7],[6,22],[23,8]
	].each{|s|
		move_item(t, s[0], s[1])
	}
	t[:key].shift
	p t[:key].size
	str = ""
	t[:key].each{|t|
		str << '"' << t << '",'
	}
	f = File.open("organize.lua", 'w')
	f.puts "ORGANIZE_KEY = {"
	f.puts str
	f.puts "}"
	f.close
end
