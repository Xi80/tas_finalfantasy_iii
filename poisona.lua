local initial_mark = nil
function initial_marked()
	initial_mark = 1
end
local battle_sum = nil
function battle_sum_get()
	battle_sum = memory.readbyte(0x0015)
	print(string.format("sum:0x%02x", battle_sum))
end
local inputport_read = nil
function inputport_read_done()
	inputport_read = emu.framecount()
end
local rgn_detected = false
local rgn_sum = 0
function rgn_sum_set()
	rgn_detected = true
	rgn_sum = memory.getregister("a")
end

function byte_to_key(v)
	local k = {}
	for i, name in ipairs({"A", "B", "select", "start", "up", "down", "left", "right"}) do 
		if AND(v, 0x80) == 0x80 then
			k[name] = true
		end
		v = v * 2
	end
	return k
end
function key_to_byte_reverse(k)
	local kk = 0
	local v = 0x01
	for i, name in ipairs({"A", "B", "select", "start", "up", "down", "left", "right"}) do 
		if k[name] == true then
			kk = OR(kk, v)
		end
		v = v * 2
	end
	return kk
end

function str_to_key(str)
	local k = {}
	for i = 1, #str, 1 do
		s = string.upper(string.sub(str, i, i))
		local x = s
		if s == "S" then
			x = "select"
		elseif s == "T" then
			x = "start"
		elseif s == "U" then
			x = "up"
		elseif s == "D" then
			x = "down"
		elseif s == "L" then
			x = "left"
		elseif s == "R" then
			x = "right"
		end
		k[x] = true
	end
	return k
end

function field_input(key)
	for i, k in ipairs(key) do
		local s = savestate.object()
		while inputport_read == nil do
			savestate.save(s)
			emu.frameadvance()
		end
		if inputport_read == emu.framecount() then
			--print(string.format("%d %d", inputport_read, emu.framecount()))
			savestate.load(s)
		end
		joypad.set(1, str_to_key(k))
		emu.frameadvance()
		inputport_read = nil
	end
end

function battle_input(key)
	for i, k in ipairs(key) do
		local keytable = str_to_key(k)
		local keybyte = key_to_byte_reverse(keytable)
		while memory.readbyte(0x0013) ~= keybyte do
			joypad.set(1, keytable)
			emu.frameadvance()
		end
	end
end

function move_main(data, x, y)
	for i, d in ipairs(data) do
		local pos = d["pos"]
		local addr
		if d["dir"] == "D" then
			addr = y
			pos = pos - 1
		elseif d["dir"] == "U" then
			addr = y
			pos = pos + 1
		elseif d["dir"] == "R" then
			addr = x
			pos = pos - 1
		elseif d["dir"] == "L" then
			addr = x
			pos = pos + 1
		end
		if x >= 0x0029 then
			pos = AND(pos, 0x3f)
		end
		local key = {d["dir"]}
		if d["extra"] ~= nil then
			key[1] = key[1] .. d["extra"]
		end
		if memory.readbyte(addr) == pos then
			--if d["last"] ~= nil then
			--	key[1] = key[1] .. d["last"]
			--end
			field_input(key)
		else
			while memory.readbyte(addr) ~= pos do
				--if math.abs(memory.readbyte(addr) - pos) == 0 and d["last"] ~= nil then
				--	key[1] = key[1] .. d["last"]
				--	print(d["last"])
				--end
				field_input(key)
			end
		end
	end
end
function move_inside(data)
	move_main(data, 0x0029, 0x002a)
end
function move_outside(data)
	move_main(data, 0x0027, 0x0028)
end
function message_window_close()
	field_input({"A", "", "B", ""})
end
function potion_use()
	--open a menu, use a potion
	field_input({"A", 
		"", "A",  "", "A",  "", "A", 
		"", "B", "", "B", "",
	})
end
function startup()
	while(initial_mark == nil) do
		emu.frameadvance()
	end
	emu.softreset()
	field_input({"", "", "A", "", "B", "A", "", "A"})
	local i = 0
	while i < (3*7) do
		field_input({"", "RA"})
		i = i + 1
	end
	field_input({"", "A", "", "ABSTDR"})
	if false then
		memory.writebyte(0x6021, 1)
		memory.writebyte(0x601c, AND(509, 0xff))
		memory.writebyte(0x601d, math.floor(509 / 256))
		memory.writebyte(0x6100, 0xb7)
		memory.writebyte(0x60c0, 0xa6)
		memory.writebyte(0x60e0, 2)
		battle_input({"A"})
		battle_input({"D", "A"})
	else
		--goblin 0xe0
		battle_input({
			"A", "", "A", "L", "D", "A",
			"", "A", "L", "A",
			"", "A", "L", "A",
			"", "A", "", "A",
		})
		i = 0
		while i < (3 + 11) do
			battle_input({"", "A"})
			i = i + 1
		end
		print("battle end")
	end
	field_input({"", "A"})
	move_inside({
		{dir = "R", pos = 0x3d},
		{dir = "U", pos = 0x03},
		{dir = "R", pos = 0x3e, extra = "A"},
	})
	--open a box
	field_input({"", "A"})
	move_inside({
		{dir = "L", pos = 0x3d},
		{dir = "L", pos = 0x3c, extra = "T"},
	})
	potion_use()
	move_inside({
		{dir = "U", pos = 0x3f},
	})
	field_input({"", "A"})
	move_inside({
		{dir = "U", pos = 0x3e},
	})
	field_input({"A"}) --移動と同時のAではダメ
	move_inside({
		{dir = "R", pos = 0x3d},
		{dir = "U", pos = 0x3d},
		{dir = "R", pos = 0x3e},
		{dir = "U", pos = 0x3b},
		{dir = "R", pos = 0x02},
		{dir = "D", pos = 0x3c, extra = "A"},
	})
	field_input({"", "A"})
	move_inside({
		{dir = "R", pos = 0x03},
		{dir = "D", pos = 0x3d},
		{dir = "R", pos = 0x06},
		{dir = "U", pos = 0x11},
		{dir = "U", pos = 0x10, extra = "T"},
	})
	potion_use()
	move_inside({
		{dir = "U", pos = 0x0a},
		{dir = "L", pos = 0x07},
		{dir = "U", pos = 0x02},
		{dir = "L", pos = 0x05},
		{dir = "U", pos = 0x00},
		{dir = "U", pos = 0x3f, extra = "A"},
	})
	field_input({"", "A"})
	move_inside({
		{dir = "D", pos = 0x02},
		{dir = "L", pos = 0x02},
		{dir = "D", pos = 0x03},
		{dir = "L", pos = 0x00},
		{dir = "L", pos = 0x3f, extra = "T"},
	})
	potion_use()
	move_inside({
		{dir = "L", pos = 0x3a},
		{dir = "U", pos = 0x3a},
	})
	field_input({"", "A"})
	move_inside({
		{dir = "U", pos = 0x07},
	})
end
function battle_landturtle()
	--A:0x8e
	field_input({"", "ATU"})

	local s = savestate.object(9)

	battle_sum = nil
	while battle_sum == nil do
		savestate.save(s)
		emu.frameadvance()
	end
	savestate.persist(s)

	--land tartule: (best:0xa6 or 0xa7)
	battle_input({"A",
		--turn 1
		"D", "", "D", "A", --にげる
		"R", "", "R", "A", --こうたい
		"R", "", "R", "A", --こうたい
		"D", "", "D", "A", --にげる
		"", "A", "", "A", "", "A", "", "A", "", "A",
		--turn 2
		"D", "", "D", "A", --ぼうぎょ
		"L", "", "L", "A", --ぜんしん
		"", "A", "", "A", --たたかう
		"D", "A",
	})
	for i = 0, 18 do
		battle_input({"", "A"})
	end
	move_inside({
		{dir = "U", pos = 0x03},
		{dir = "U", pos = 0x02, extra = "A"},
	})
	for i = 0, 4 do
		field_input({"", "A"})
	end
	move_inside({
		{dir = "L", pos = 0x3e},
		{dir = "U", pos = 0x3e},
		{dir = "R", pos = 0x3f},
	})
end
function second_dungeon()
	move_outside({
		{dir = "D", pos = 0x1c},
		{dir = "U", pos = 0x1b},
	})
	field_input({"", "A"})
	move_inside({
		{dir = "D", pos = 0x3f},
		{dir = "D", pos = 0x02},
		{dir = "R", pos = 0x16},
	})
	--emu.speedmode("normal")
	--最短の宝箱操作は町人ブロックに影響するらしいのでいまのところ残す
	--field_input({"U", "A", "", "B", ""})
	field_input({"UA", "", "A"})
	move_inside({
		{dir = "R", pos = 0x17},
	})
	--field_input({"U", "A", "", "B", ""})
	field_input({"UA", "", "A"})
	move_inside({
		{dir = "L", pos = 0x3c},
		{dir = "U", pos = 0x3a},
	})
	move_outside({
		{dir = "D", pos = 0x22},
	})
	field_input({"", "A"})
end
function uru_maho_bougu_buki()
	move_inside({
		{dir = "U", pos = 0x15},
		{dir = "U", pos = 0x14, extra = "A"},
	})
	field_input({"", "A"})
	move_inside({
		{dir = "L", pos = 0x07},
		{dir = "U", pos = 0x0e},
		{dir = "L", pos = 0x03},
		{dir = "U", pos = 0x3f},
		{dir = "U", pos = 0x3e, extra = "A"},
	})
	--魔法屋
	field_input({
		"", "A", "R", "A",
		"", "A", --ポイゾナ4
		"B", "", "B", "", "B",
	})
	move_inside({
		{dir = "D", pos = 0x0d},
		{dir = "R", pos = 0x05},
		{dir = "D", pos = 0x0e},
		{dir = "R", pos = 0x07},
		{dir = "U", pos = 0x00},
		{dir = "U", pos = 0x3f, extra = "A"},
	})
	--防具屋
	field_input({
		"", "A", "R", "A",
		"U", "LA",
		"D", "RA",
		"D", "RA", "", "A", "", "A", --かわのたて 4x3
		"B", "LA",
		"D", "RA", "", "A", --どうのうでわ x2
		"U", "A", "", "A", "", "A", --かわのぼうし x3
		"U", "LA",
		"B", "", "B", "", "B"
	})
	move_inside({
		{dir = "D", pos = 0x0e},
		{dir = "R", pos = 0x0b},
		{dir = "U", pos = 0x12},
		{dir = "U", pos = 0x11, extra = "A"},
	})
	--武器屋
	field_input({
		"", "A", "", "A",
		"D", "RA", 
		"B", "RA",
		"U", "A", "", "A", "", "A", --ダガー 4x3
		"D", "RA", --つえ 4
		"D", "A", --ヌンチャク 4
		"B", "", "B", "", "B"
	})
	move_inside({
		{dir = "D", pos = 0x12, extra = "T"},
	})
	field_input({"A"})
end
function uru_bougu_maho_buki()
	move_inside({
		{dir = "U", pos = 0x15},
		{dir = "U", pos = 0x14, extra = "A"},
	})
	field_input({"", "A"})
	move_inside({
		{dir = "L", pos = 0x07},
		{dir = "U", pos = 0x3f},
		{dir = "U", pos = 0x3e, extra = "A"},
	})
	--防具屋
	field_input({
		"", "A", "", "A",
		"", "A", "", "A", --ふく
		"D", "A", --かわよろい
		"U", "LA", --どうのうでわ
		"U", "A", "", "A", --かわのぼうし
		"U", "A", "", "A",
		"B", "LA",
		"", "A",
		"B", "", "B", "", "B"
	})
	move_inside({
		{dir = "D", pos = 0x0e},
		{dir = "L", pos = 0x03},
		{dir = "U", pos = 0x3f},
		{dir = "U", pos = 0x3e, extra = "A"},
	})
	--魔法屋
	field_input({
		"", "A", "R", "A",
		"", "A", --ポイゾナ4
		"B", "", "B", "", "B",
	})
	move_inside({
		{dir = "D", pos = 0x0d},
		{dir = "R", pos = 0x05},
		{dir = "D", pos = 0x0e},
		{dir = "R", pos = 0x0b},
		{dir = "U", pos = 0x12},
		{dir = "U", pos = 0x11, extra = "A"},
	})
	--武器屋
	field_input({
		"", "A", "", "A",
		"U", "A", "", "A", --ヌンチャク
		"U", "A", "", "A", "", "A",  --つえ
		"B", "RA",
		"", "A",
		"U", "A", --ロングソード
		"U", "A", "", "A", "", "A",
		"B", "", "B", "", "B"
	})
	move_inside({
		{dir = "D", pos = 0x12, extra = "T"},
	})
	field_input({"A"})
end
function uru_buki_bougu_maho()
	move_inside({
		{dir = "U", pos = 0x14},
	})
	message_window_close()
	move_inside({
		{dir = "R", pos = 0x0b},
		{dir = "U", pos = 0x0d},
	})
	move_inside({
		{dir = "U", pos = 0x11},
	})

	local organize = 16
	--武器屋
	--ダガー4x3 648, ダガーx10+2 601
	if organize == 17 then
		field_input({"A", 
			"", "A", "L", "A",
			"D", "A", 
			"B", "RA",
			"", "A", "", "A", --ダガー 2
			"B", "RA",
			"D", "A", --ロングソード4
			"D", "A", --つえ 4
			"D", "A", --ヌンチャク 4
			"B", "", "B", "", "B"
		})
	else
		field_input({"A", 
			"", "A", "L", "A",
			"D", "A", 
			"B", "RA",
			"", "A", "", "A", --ダガー 2
			"D", "A", --ロングソード1
			"B", "RA",
			"D", "A", --つえ 4
			"D", "A", --ヌンチャク 4
			"B", "", "B", "", "B"
		})
	end
	move_inside({
		{dir = "D", pos = 0x0e},
		{dir = "L", pos = 0x07},
		{dir = "U", pos = 0x3f},
	})
	--防具屋
	--かわのたてx4x3 432, かわのたてx10+3 361
	if organize == 17 then
--ポーション:1 かわのたて:13-2
--ダガー:12 ロングソード:4
--つえ:4 ヌンチャク:4
--ふく:1 かわよろい:1
--かわのぼうし:7 どうのうでわ:2
--ポイゾナ:4
		field_input({"A", 
			"", "A", "", "A",
			"", "A", --ふく1
			"D", "A", --かわよろい1
			"B", "RA",
			"D", "RA", --かわのぼうし4
			"B", "RA",
			"U", "A", --かわのたて10
			"B", "RA",
			"", "A", "", "A", --かわのたて2
			"D", "A", "", "A", "", "A", --かわのぼうし3
			"R", "A", "", "A" --どうのうでわ2
		})
	else
--ポーション:1 かわのたて:13
--ダガー:12 ロングソード:1
--つえ:4 ヌンチャク:4
--ふく:4 かわのぼうし:7
--どうのうでわ:2 かわよろい:1
--ポイゾナ:4
		field_input({"A", 
			"", "A", "R", "A",
			"", "A",
			"U", "LA",
			"U", "A", "", "A", "", "A", --かわのたて 4x3
			"B", "LA",
			"D", "A", "", "A", "", "A",
			"D", "A", "", "A",
			"D", "RA"
		})
	end
	field_input({
		"B", "", "B", "", "B"
	})
	--魔法屋
	move_inside({
		{dir = "D", pos = 0x0e},
		{dir = "L", pos = 0x03},
		{dir = "U", pos = 0x3e},
	})
	field_input({"A", 
		"", "A", "R", "A",
		"", "A", --ポイゾナ4
		"B", "", "B", "", "B",
		"", --町人ブロック回避
	})
	move_inside({
		{dir = "D", pos = 0x3f, extra = "T"},
	})
	field_input({"A"})
end
dofile("organize.lua")
function equipment()

	local t = emu.framecount()
	field_input(ORGANIZE_KEY)
	print(string.format("organize:%d frames", emu.framecount() - t))

	--emu.speedmode("normal")
	field_input({
		"B",
		"D", "RA",
		"", "DA",
	})
	local righthand = "R"
	local lefthand = ""
	field_input({
		"U", "A", "", "A", "D", "A", righthand, "A",
		"D", "A", lefthand, "A", "B", 
	})
--	local righthand = ""
--	local lefthand = "R"
--	field_input({
--		"U", "A", "", "A",
--		"D", "A", righthand, "A",
--		"D", "A", lefthand, "A", "B", 
--	})
	field_input({
		"", "BT"
	})
end
function goto_monster()
	--emu.speedmode("normal")
	local down_first = 0x0e
	if memory.readbyte(0x0029) == 0x3c and memory.readbyte(0x002a) == 0x12 then
		down_first = 0x0d
	end
	move_inside({ 
		--{dir = "D", pos = 0x0d, extra = "T"},
		--{dir = "R", pos = 0x05, extra = "T"},
		{dir = "D", pos = down_first, extra = "T"},
		{dir = "R", pos = 0x0d, extra = "T"},
		{dir = "U", pos = 0x08, extra = "T"},
		{dir = "L", pos = 0x0c, extra = "T"},
		{dir = "U", pos = 0x04, extra = "T"},
		{dir = "R", pos = 0x0f, extra = "T"},
		{dir = "U", pos = 0x3e, extra = "T"},
		{dir = "L", pos = 0x0e, extra = "T"},
		{dir = "R", pos = 0x0f, extra = "UDTBA"},
	})
	memory.writebyte(0x6100, 0)
	battle_input({"A"})
end
function final_battle()
	local weapon_swap_first = ""
	battle_input({"U", "A", "D", "", "D", "A", "", "A", "", "A"})
	if false then
		weapon_swap_first = "D"
	end
	local i = 0
	local item_swap_task = {"U", "A", weapon_swap_first, "A", "L", "A"}
	local num = memory.readbyte(0x60c1) + memory.readbyte(0x60c3) - 2
	
	while i < num do
		battle_input(item_swap_task)
		if item_swap_task[3] == "" then
			item_swap_task[3] = "D"
		else
			item_swap_task[3] = ""
		end
		i = i + 1
	end
	battle_input({"", "B", "", "B", "U", "A"})
end
---- start ----
memory.registerexecute(0xc0b6, 1, initial_marked)
memory.registerexecute(0xfc30, 1, battle_sum_get)
memory.registerexecute(0xd299, 1, inputport_read_done)
--memory.registerexecute(0xfbd2, 1, inputport_read_done)

local phase = 4
emu.unpause()
if phase == 0 then
	emu.speedmode("turbo")
	movie.record("e:/hoge")
	phase = phase + 1
elseif phase == 1 then
	emu.speedmode("turbo")
	emu.poweron()
end

for i, func in pairs({startup, battle_landturtle, second_dungeon, uru_bougu_maho_buki, equipment, goto_monster}) do
	local s = savestate.object(i)
	if phase <= i then
		local before = emu.framecount()
		func()
		print(string.format("phase %d: %d frames", i, emu.framecount() - before))
		if movie.recording() == false then
			savestate.save(s)
			savestate.persist(s)
		end
	else
		savestate.load(s)
	end
end

final_battle()
local seconds = emu.framecount() / 60.0988
print(string.format("done %d frames, %d m %d sec", emu.framecount(), math.floor(seconds / 60), math.floor(seconds % 60)))
if movie.recording() == true then
	movie.stop()
end
