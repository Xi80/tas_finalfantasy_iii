require 'win32/clipboard'
def tasvideos(filename)
	str = ''
	File.open(filename).each{|t|
		if t =~ /\A\*+/
			t.gsub!('***', '!')
			t.gsub!('**', '!!')
			t.gsub!('*', '!!!')
			t.gsub!(/ \[.+\]\n/, "\n")
		elsif t =~ /\A\-/
			t.gsub!('-', '*')
		elsif t =~ /\A\+/
			t.gsub!('+', '#')
		end
		str += t
	}
	Win32::Clipboard.set_data(str, Win32::Clipboard::UNICODETEXT)
end

def hatenablog(filename)
	str = ''
	pre = 0
	paragraph = 0
	visible = Array.new(4, true)
	level = 0
	File.open(filename).each{|t|
		if t =~ /\A\*+/
			if t =~ /\*\*\*+/
				paragraph = 3
			elsif t =~ /\*\*/
				paragraph = 2
			else
				paragraph = 1
			end
			v = true
			if t.include? '@'
				cc = t.chomp.split('@')
				v = cc[1] == 'H'
				t = cc[0] + "\n"
			end
			if visible[paragraph - 1] == true
				(paragraph..(visible.size - 1)).each{|i|
					visible[i] = v
				}
			end
			#p t,v,paragraph, visible
		elsif t =~ /\A[^ \t]/
			case pre
			when 1
				#t = "||<\n" + t
				t = "@@!!@@\n" + t
			end
			pre = 0
		elsif t =~ /\A[ \t]/
			#p t
			case pre
			when 0
				t = ">||\n" + t
				pre = 1
			end
		end
		
		if pre == 0
			t.gsub!('>', '&gt;')
			t.gsub!('<', '&lt;')
			t.gsub!('@@!!@@', '||<')
		end
		
		if visible[paragraph]
			str += t
		end
	}
	Win32::Clipboard.set_data(str, Win32::Clipboard::UNICODETEXT)
end
begin
	case ARGV.shift
	when 'tasvideos'
		tasvideos(ARGV.shift)
	when 'hatenablog'
		hatenablog(ARGV.shift)
	else
		puts "[tasvideos|hatenablog] [src pukiwiki text file]"
		exit
	end
end
